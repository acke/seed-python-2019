arr = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

NR = len(arr)
NC = len(arr[0])

sum_arr = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
]

def sum_neighbors_bad(r, c, arr):
    total = 0
    if r != 0 and c != 0:
        total += arr[r-1][c-1]
    if r != 0:
        total += arr[r-1][c]
    if r != 0 and c != NC-1:
        total += arr[r-1][c+1]
    if c != 0:
        total += arr[r][c-1]
    if c != NC-1:
        total += arr[r][c+1]
    if r != NR-1 and c != 0:
        total += arr[r+1][c-1]
    if r != NR-1:
        total += arr[r+1][c]
    if r != NR-1 and c != NC-1:
        total += arr[r+1][c+1]
    return total

dr = [-1, -1, -1, 0, 0, 1, 1, 1]
dc = [-1,  0,  1,-1, 1,-1, 0, 1]
def sum_neighbors(r, c, arr):
    total = 0
    for i in range(8):
        x, y = r + dr[i], c + dc[i]
        if x < 0 or x >= NR or y < 0 or y >= NC:
            continue
        total += arr[x][y]
    return total

for r in range(3):
    for c in range(3):
        sum_arr[r][c] = sum_neighbors(r, c, arr)

print(arr)
print(sum_arr)
