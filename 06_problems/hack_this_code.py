# Let's play a betting game
from random import randint

num_to_move = ["rock", "paper", "scissors"]

beats = {
    "rock": "scissors",
    "paper": "rock",
    "scissors": "paper"
}

def winner(choice1, choice2):
    if choice1 == choice2:
        return 0 # tie
    if beats[choice1] == choice2:
        return 1 # player 1 wins
    else:
        return 2 # player 2 wins

cash = 100
while True:
    print()
    print("You have {} dollars.".format(cash))
    if cash > 1000000:
        print("Wow, you are rich! Congratulations!")
        break
    print("How much do you want to bet?")
    bet = int(input())
    if bet > cash:
        print("You can't bet more than what you have!")
        continue
    print("rock, paper, or scissors?")
    choice = input().lower().strip()
    bot_choice = num_to_move[randint(0,2)]
    print("I chose {}.".format(bot_choice))
    print()
    result = winner(choice, bot_choice)
    if result == 0:
        print("Tie!")
    elif result == 1:
        print("You win!")
        cash += bet
    else:
        print("You lose!")
        cash -= bet


