from random import randint

print("Lottery Rules:")
print("Lotto is consisted of 6 numbers from 1 to 45")
print("Match as many numbers as possible; lotto is chosen randomly")
print("If you match all, you win! Prob is 1/8145060! Good luck!")
print("Enter your 6 numbers for lotto ticket:")
entry = [int(i) for i in input().split()]

lotto = []
for _ in range(6):
    lotto.append(randint(1,45))
match = 0
for i in range(6):
    for j in range(6):
        if lotto[i] == entry[j]:
            match += 1
if match == 6:
    print("You win the lottery!")
else:
    print("Better luck next time!")

