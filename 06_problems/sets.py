# Sets in Python
# Recall dictionaries

# maps Type -> Type
# can map string to integer
"""
d = {'a': 5, 'b': 6}
print(d['a'])
print(d['b'])
print(d)
d['a'] = 10
print(d['a'])
print(d)
"""
# How many unique characters in a given word?
# vaccuum

# Idea: take advantage of uniqueness in dict
# however, we do not need the "value", only "key"
# this is where sets come in

# advantages of set:
# 1. uniqueness of keys (can get rid of duplicates)
# 2. very fast to check membership of (val in set)
# 3. can perform operations like + and - on sets

word = "vaccuum"
s = set()
# to initialize a set
# s_init = {1,2,3,4,5} - {3,4,7,8}
for c in word:
    s.add(c)
print(len(s))

# here is a one liner
print(len(set(s)))

