# Given a bunch of integer pairs that represent
# start and end, how do we determine if we are at
# one of the starts? How do we determine where we
# end up at?

# 5 players, keep locations in array
p_locs = [1] * 5

# given 5 players, how do I go through each player's index
# whenever you see a notion of cycling through,
# think modulus, % operator
dice_rolls = [1,2,3,1,2,3,6,5,4,2,3,1,4,5,2,3,1,6]
print(p_locs)
for i, roll in enumerate(dice_rolls):
    p_locs[i % 5] += roll
print(p_locs)

# use a dictionary
# move[start] = end
# pretend we have a ladder from 2 to 20, 25 to 76
# and snakes from 50 to 21, 87 to 3
move = {2: 20, 25: 76, 50: 21, 87: 3}
# let pos be position of a player
# checking existence in dictionary is fast!
pos = 25
if pos in move:
    pos = move[pos]

# direct addressing hash table
# idea is pretty simple
move2 = list(range(101))
move2[2] = 20
move2[25] = 76
move2[50] = 21
move2[87] = 3

pos = 25
pos = move2[pos]

