T = int(input())
for c in range(T):
    speeds = [int(i) for i in input().split()]
    print("Case {}: {}".format(c+1, max(speeds[1:])))
