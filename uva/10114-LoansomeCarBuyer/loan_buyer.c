#include <stdio.h>

int main(){
	int num_months, R;
	float down_pay, loan_amt;
	while (scanf("%d %f %f %d", &num_months, &down_pay, &loan_amt, &R), num_months >= 0){
		float amt_owed = loan_amt, car_worth = loan_amt + down_pay;
		int curr_month;
		float rdep[num_months + 1];
		scanf("%d %f", &curr_month, rdep);
		R--;
		int tmpmonth;
		float tmpdep;
		int i;
		car_worth *= 1 - rdep[0];
		if (amt_owed < car_worth){
			printf("0 months\n");
			while (R--){
				scanf("%d %f", &tmpmonth, &tmpdep);
			}
			continue;
		}
		for (i = 0; i <= num_months; i++)
			rdep[i] = rdep[0];
		while (R--){
			scanf("%d %f", &tmpmonth, &tmpdep);
			for (i = tmpmonth; i <= num_months; i++)
				rdep[i] = tmpdep;
		}
		curr_month = 1;
		while (curr_month < num_months){
			amt_owed -= loan_amt / num_months;
			car_worth *= 1 - rdep[curr_month];
			if (amt_owed < car_worth)
				break;
			curr_month++;
		}
		printf("%d %s\n", curr_month, curr_month == 1 ? "month" : "months");
	}
	return 0;
}
