while True:
    duration, down_payment, loan_amt, num_records = input().split()
    duration = int(duration)
    if duration < 0:
        break
    down_payment = float(down_payment)
    loan_amt = float(loan_amt)
    num_records = int(num_records)
    
    monthly_payment = loan_amt / duration
    car_worth = loan_amt + down_payment
    depr_records = [0] * (duration + 1)
    for _ in range(num_records):
        record = input().split()
        tmp_month = int(record[0])
        tmp_depr = float(record[1])
        for i in range(tmp_month, duration+1):
            depr_records[i] = tmp_depr
    for month in range(duration+1):
        car_worth *= 1 - depr_records[month]
        if loan_amt < car_worth:
            if month == 1:
                print("1 month")
            else:
                print("{} months".format(month))
            break
        loan_amt -= monthly_payment

