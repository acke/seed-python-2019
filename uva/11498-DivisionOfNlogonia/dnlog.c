#include <stdio.h>

int main(){
	int K, N, M, X, Y;
	while (scanf("%d",&K), K){
		scanf("%d %d", &N, &M);
		while (K--){
			scanf("%d %d", &X, &Y);
			if (X == N || Y == M){
				printf("divisa\n");
				continue;
			}
			if (X < N){
				if (Y < M)
					printf("SO\n");
				else
					printf("NO\n");
			}
			else{
				if (Y < M)
					printf("SE\n");
				else
					printf("NE\n");
			}
		}
	}
	return 0;
}
