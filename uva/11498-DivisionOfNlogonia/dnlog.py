while True:
    K = int(input())
    if K == 0:
        break
    N, M = [int(n) for n in input().split()]
    for _ in range(K):
        X, Y = [int(n) for n in input().split()]
        if X == N or Y == M:
            print("divisa")
            continue
        if X < N:
            if Y < M:
                print("SO")
            else:
                print("NO")
        else:
            if Y < M:
                print("SE")
            else:
                print("NE")

