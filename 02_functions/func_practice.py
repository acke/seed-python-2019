# Write a function to compute fact(n) = n!
# fact(5) = 5 * 4 * 3 * 2 * 1
# Write a function to compute exp(n) = 2^n
# exp(5) = 2^5
# Write a function to compute n!/2^n
def fact(n):
    prod = 1
    for i in range(2,n+1):
        prod *= i
    return prod

def exp(n):
    return 2**n

def foo(n):
    return fact(n) / exp(n)

# Write a function to compute triangle(n)
# triangle(n) = n + (n-1) + (n-2) + ... + 2 + 1
# Write a function to compute cube_n(n):
# cube_n(n) = n^3 + (n-1)^3 + ... + 2^3 + 1^3
# Write a function to compute the triangle_sq(n):
# triangle_sq(n) = (n + (n-1) + (n-2) + ... + 2 + 1)^2
# Check if cube_n(n) == triangle_sq(n) for n = 1 to 100

# Write a function leibniz(n):
# 4 * (1 - (1/3) + (1/5) - (1/7) + (1/9) - ..) up to n terms
# For what value of n does this series equal math.pi?

