# Functions: input -> output

# max(5,7)
# function: takes in two numbers and outputs max

# print n^4 + n^3 + n^2 + n + 1 for the first 10 numbers
# for the numbers 90 to 100
# for the numbers 200 to 210

def foo(n):
    return n**4 + n**3 + n**2 + n + 1

for i in range(10):
    print(foo(i))
for i in range(90,101):
    print(foo(i))
for i in range(200,211):
    print(foo(i))

