# Print out all pairs of integers from 0 to 9
# (0,0), (0,1), (0,2), .., (0,9), 
# (1,0), (1,1), (1,2), .., (1,9)
# ...
# (9,0), (9,1), (9,2), .., (9,9)

for i in range(10):
    for j in range(10):
        print("  ", i, j)

# Pretend we have a password that is 4 characters long
# doge

password = "doge"

def hack(password):
    letters = "abcdefghijklmnmopqrstuvwxyz"
    for c1 in letters:
        for c2 in letters:
            for c3 in letters:
                for c4 in letters:
                    test_pw = c1 + c2 + c3 + c4
                    print(test_pw)
                    if test_pw == password:
                        return

hack(password)

