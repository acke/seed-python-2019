# Write a program that does the following:

# User inputs an integer n
# Print a pyramid that looks like follows
"""
3
#
##
###

5
#
##
###
####
#####

0
"""
while True:
    n = int(input())
    if n == 0:
        break
    for i in range(1,n+1):
        print("#"*i)

