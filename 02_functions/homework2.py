# Homework 2
import math
"""
For your enjoyment, to read more about functions:

https://automatetheboringstuff.com/chapter3/

"""
"""
Write a function to compute triangle(n)
triangle(n) = n + (n-1) + (n-2) + ... + 2 + 1

Write a function to compute cube_n(n):
cube_n(n) = n^3 + (n-1)^3 + ... + 2^3 + 1^3

Write a function to compute the triangle_sq(n):
triangle_sq(n) = (n + (n-1) + (n-2) + ... + 2 + 1)^2

Check if cube_n(n) == triangle_sq(n) for n = 1 to 100
"""
def triangle(n):
    return n*(n+1)//2

def cube_n(n):
    total = 0
    for i in range(1,n+1):
        total += i**3
    return total

def triangle_sq(n):
    return triangle(n)**2

# indeed cube_n == triangle_sq

"""
Write a function leibniz(n):
4 * (1 - (1/3) + (1/5) - (1/7) + (1/9) - ..) up to n terms
For what value of n does this series equal math.pi?
"""
def leibniz(n):
    total = 0
    for i in range(1,n+1):
        total += (-1)**(i+1) * 4 / (2*i-1)
    return total

"""
Write the prime checking function
Try to do it from scratch; you can refer to my example if you get stuck
***
Try to make it faster (consider checking less numbers)
"""
def is_prime(n):
    if n == 1:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    for i in range(3, int(math.sqrt(n)) + 2,2):
        if n % i == 0:
            return False
    return True
"""***
Print out all pairs from 0 to 9 in increasing order only
(0,0), (0,1), ..., (0,8), (0,9)
(1,1), (1,2), ..., (1,8), (1,9)
(2,2), (2,3), ..., (2,9)
(3,3), ...
"""

for i in range(10):
    for j in range(i,10):
        print(" ", i,j)


