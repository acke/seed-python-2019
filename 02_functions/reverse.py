# Write a function called reverse
# input is a string
# output is a string with the contents reversed
# reverse("hello") -> "olleh"

def reverse(s):
    r = ""
    for i in range(len(s)-1,-1,-1):
        r += s[i]
    return r

print(reverse("hello"))

