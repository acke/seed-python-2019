# Warmup Exercises on Loops

import math

# Maintaining some right answer so far in a candidate answer variable
# Formally, known as "loop invariant"

# SUM OF FIRST N ODD CUBES:
# sum_n_odd_cube(n)
# Given n, output the sum of the first n odd cubes
# ex. n = 3: 1 + 27 + 125 = 153

def sum_n_odd_cube(n):
    total = 0
    for i in range(1,n+1):
        total += (2*i-1)**3
    return total

# MINIMUM EXERCISE:
# Read in arbitrary number of positive integers from user
# Once they enter 0, print out the minimum of all the other numbers they inputed

"""
min_so_far = math.inf
while True:
    n = int(input())
    if n == 0:
        print(min_so_far)
        break
    if n < min_so_far:
        min_so_far = n
"""

# SECOND SMALLEST:
# Read in arbitrary number of positive integers from user
# Once they enter 0, print out the second smallest of the other numbers they inputed
# This is possible with only a few variables.

sec_small = math.inf
smallest = math.inf
while True:
    n = int(input())
    if n == 0:
        print()
        break
    if n < smallest:
        sec_smallest = smallest
        smallest = n
    elif n < sec_small:
        sec_small = n
print(sec_small)



