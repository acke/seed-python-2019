arr = [
    5,
    8,
    5,
    6,
    1,
    2,
    5,
    3,
    2,
    8,
    9,
    8,
    7,
    1,
    7,
    5,
    5
]

# Dictionary
freq = {}
for val in arr:
    if val not in freq:
        freq[val] = 1
    else:
        freq[val] += 1

for key in freq:
    print(" ",key, freq[key])

