python_para = "Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects. Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, including procedural, object-oriented, and functional programming. Python is often described as a batteries included language due to its comprehensive standard library. Python was conceived in the late 1980s as a successor to the ABC language. Python 2.0, released 2000, introduced features like list comprehensions and a garbage collection system capable of collecting reference cycles. Python 3.0, released 2008, was a major revision of the language that is not completely backward-compatible, and much Python 2 code does not run unmodified on Python 3. Due to concern about the amount of code written for Python 2, support for Python 2.7 (the last release in the 2.x series) was extended to 2020. Language developer Guido van Rossum shouldered sole responsibility for the project until July 2018 but now shares his leadership as a member of a five-person steering council. Python interpreters are available for many operating systems. A global community of programmers develops and maintains CPython, an open source reference implementation. A non-profit organization, the Python Software Foundation, manages and directs resources for Python and CPython development."

# Frequency table on python_para letters
# Determine the order of most frequent to least frequent letters. 
# Output the frequencies as a percentage out of 100.

from string import ascii_lowercase

freq = {}

for letter in ascii_lowercase:
    freq[letter] = 0

for c in python_para:
    if not c.isalpha():
        continue
    c = c.lower()
    freq[c] += 1

for c in freq:
    freq[c] /= len(python_para)
    freq[c] *= 100

sorted_freq = []
for c in freq:
    sorted_freq.append((freq[c], c))
sorted_freq.sort(reverse=True)

for freq, c in sorted_freq:
    print(" ", c, freq)

