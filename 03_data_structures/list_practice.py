# Let's write a quiz statistic program

# How many quizzes are you inputting?
# 6
# Please enter your 6 quiz grades.
# 95
# 75
# 80
# 85
# 90
# 85
# Here are your statistics:
# Max: 95
# Min: 70
# Range: 26
# Mean: 85
# Median: 85
# Mode: 85

# Hint for median: Use .sort()
# Challenge: Try to do Mode also

print("How many quizzes are you inputting?")
n = int(input())
print("Please enter your " + str(n) + " quiz grades.")
quiz_scores = []
for _ in range(n):
    score = int(input())
    quiz_scores.append(score)
print("Here are your statistics:")
print("Max:", max(quiz_scores))
print("Min:", min(quiz_scores))
print("Range:", max(quiz_scores) - min(quiz_scores))
print("Mean:",sum(quiz_scores)/n)
quiz_scores.sort()
if n % 2 == 0:
    median = (quiz_scores[n//2-1] + quiz_scores[n//2]) / 2
else:
    median = quiz_scores[n//2]
print("Median:",median)

freq = {}
for quiz_score in quiz_scores:
    if quiz_score not in freq:
        freq[quiz_score] = 1
    else:
        freq[quiz_score] += 1

"""
max_key = 0
max_val = 0
for key in freq:
    val = freq[key]
    if val > max_val:
        max_val = val
        max_key = key
"""
freq_scores = []
for key in freq:
    freq_scores.append((freq[key],key))
freq_scores.sort(reverse=True)
print(freq_scores)
#print("Mode:",max_key,max_val)



