# Homework 3

# For any problem you couldn't do in class or didn't understand clearly,
# implement them again without looking at the code
# Refer to it only if you really get stuck. You want the concepts to stick.

# COLLATZ CONJECTURE
# For all integers n, the following process will terminate
# if n = 1: stop
# if n is even, divide by 2
# if n is odd, multiply by 3 and add 1
# User inputs a number n
# Given n, return number of steps before terminating
# Keep track of the maximal value during this process and print that as well
n = 10000
steps = 0
max_val = n
while n > 1:
    if n % 2 == 0:
        n /= 2
    else:
        n = 3 * n + 1
    if n > max_val:
        max_val = n
    steps += 1


# FIBONACCI NUMBERS
# fib(n)
# Given n, output the nth Fibonacci number
# ex. n = 0: 0, n = 1: 1, n = 2: 1, n = 3: 2

def fib(n):
    a = 0
    b = 1
    c = 1
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        for _ in range(n-2):
            a, b, c = b, c, a+b
        return c

def fib_rec(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-2) + fib(n-1)

# BRUTE FORCE GCD:
# gcd(a, b)
# Given two numbers, compute their greatest common divisor
# Idea: think about the largest possible GCD and work your way down and check
# if your candidate divides both.

def gcd(a,b):
    for cand in range(min(a,b), 1, -1):
        if a % cand == 0 and b % cand == 0:
            return cand
    return 1

# LUCAS SEQUENCE:
# lucas(p, q, n)
# Lucas sequence is a generalization of Fibonacci sequence
# L_0 = 0
# L_1 = 1
# L_n = p * L_{n-1} - q * L_{n-2}
# if p = 1 and q = -1, then this is just Fibonacci
# Write a function that takes in p,q,n and outputs nth Lucas number

# MAX GAP:
# max_gap(arr)
# Given an array of integers, find the largest "gap", meaning the
# largest absolute difference in value between any two consecutive elements.
# ex. [5,8,1,2,-5,3,9] => 8 (between -5 and 3)

def max_gap(arr):
    largest_gap = -1
    for i in range(1,len(arr)):
        diff = abs(arr[i] - arr[i-1])
        if diff > largest_gap:
            largest_gap = diff
    return largest_gap

# PYTHON PARAGRAPH
# In different file

