"""
q1 = 7
q2 = 9
q3 = 8
q4 = 1
q5 = 0
q6 = 5

"Hacky" alternative
q = "798105"
total = 0
for c in q:
    total += int(c)
print(total/len(q))
"""

quiz_scores = [7,9,8,1,2,5]
print(quiz_scores)
print(quiz_scores[0])
print(len(quiz_scores))
print(quiz_scores[len(quiz_scores)-1])
print(quiz_scores[-1])
print(7 in quiz_scores)

quiz_scores.append(6) # appends to the end of the list
quiz_scores.pop() # deletes and returns last element

product = 1
for i in range(len(quiz_scores)):
    product *= quiz_scores[i]
print(product)

product = 1
for quiz_score in quiz_scores:
    product *= quiz_score
print(product)

product = 1
for i, quiz_score in enumerate(quiz_scores):
    product *= quiz_score
print(product)

