def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    return fib(n-1) + fib(n-2)

for i in range(10):
    print(fib(i))

def fact(n):
    if n == 0:
        return 1
    return n * fact(n-1)

def exp(b, e):
    if e == 0:
        return 1
    return b * exp(b, e-1)

# 0, 1, 2 are the pegs
def hanoi(n, s, e):
    if n == 1:
        print(s, e)
    else:
        hanoi(n-1, s, 3 - s - e)
        print(s, e)
        hanoi(n-1, 3 - s - e, e)

hanoi(3, 0, 2)

