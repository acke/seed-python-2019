# Some puzzles to consider

1. A mythical city contains 100,000 married couples but no children. Each family wishes to "continue the male line", but they do not wish to over-populate. So, each family has one baby per annum until the arrival of the first boy. For example, if (at some future date) a family has five children, then it must be either that they are all girls, and another child is planned, or that there are four girls and one boy, and no more children are planned. Assume that children are equally likely to be born male or female.

  Let p(t) be the percentage of children that are male at the end of year t. How is this percentage expected to evolve through time?

2. There are 100 light bulbs lined up in a row in a long room. Each bulb has its own switch and is currently switched off. The room has an entry door and an exit door. There are 100 people lined up outside the entry door. Each bulb is numbered consecutively from 1 to 100. So is each person.

  Person No. 1 enters the room, switches on every bulb, and exits. Person No. 2 enters and flips the switch on every second bulb (turning off bulbs 2, 4, 6...). Person No. 3 enters and flips the switch on every third bulb (changing the state on bulbs 3, 6, 9...). This continues until all 100 people have passed through the room.

  And how many of the light bulbs are illuminated after the 100th person has passed through the room?

3. What is the smallest positive integer that leaves a remainder of 1 when divided by 2, remainder of 2 when divided by 3, a remainder of 3 when divided by 4, and so on up to a remainder of 9 when divided by 10?

4. We are to play a version of Russian Roulette, the revolver is a standard six shooter but I will put two bullets in the gun in consecutive chambers. I spin the chambers, put the gun to my head pull the trigger and survive. I hand you the gun and give you a choice...
You may put the gun straight to your head and pull the trigger, or you may re-spin the gun before you do the same.

  What is your choice and why? How does this differ from the case with only one bullet?

5. Three ants are sitting at the three corners of an equilateral triangle. Each ant randomly picks a direction and starts to move along the edge of the triangle. What is the probability that none of the ants collide?

    How about other shapes, Square? N sides?

6. Monty Hall Problem - You are in a game show with a game host that presents you three doors. Behind two of them, there is a goat. Behind one, there is a car. You want the car. First, you get to select one of the three doors. Then, the game host reveals a door that contains a goat out of the two remaining doors you did not select. Note that this will always be possible (if you chose a door initially, the host will randomly select one of the other doors to open). Finally, you are given the option to stay with your initial choice or switch to the other door. Is there an advantage to swapping or does it not matter?

7. Simple nim game variant - There are some number of dragons on the field. Every dragon except one is red. Each turn, you are allowed to take 1, 2, or 3 dragons. There is an opponent who is allowed to do the same. The goal is to take the red dragon, but you are only allowed to take it if there are no other dragons left to take. There is a misere variant in which you must force the enemy to take the red dragon. Can you find a winning strategy? Given the number of total dragons and the variant, can you determine who (first or second player) wins with perfect play?

8. Alice, Bob, and Cheri, three smart people, arrange a three-way duel. Alice is a poor shot, hitting her target only 1/3 of the time on average. Bob is better, hitting his target 2/3 of the time. Cheri is a sure shot hitting 100% of the time. They take turns shooting, first Alice, then Bob, the Cheri, then back to Alice, and so on until one is left. What is Alice's best course of action? And her chances of survival?

9. There is a long line of people waiting outside a theatre to buy tickets. The theatre owner comes out and announces that the first person to have a birthday same as someone standing anywhere before him in the line gets a free ticket. Where will you stand to maximize your chance?

10. Secretary/Marriage/Dowry problem. Many names for the same problem - I'll describe it in the context of marriage. You are given a choice of n suitors, where n can any positive integer (n is given). Each suitor can be ranked comparatively to one another and if you were to have seen them all lined up, you would know clearly the rankings of each person. However, you are presented a suitor one at a time, and you must make the decision on the spot right after seeing the person. You know their relative rank to everyone you've seen, but are not sure if they are the best out of all the suitors. What is the best strategy to choose the first ranked suitor?

11. You have 52 playing cards (26 red, 26 black). You draw cards one by one. A red card pays you a dollar. A black one fines you a dollar. You can stop any time you want. Cards are not returned to the deck after being drawn. What is the optimal stopping rule in terms of maximizing expected payoff?

