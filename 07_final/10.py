from random import shuffle

def marriage_problem():
    suitors = list(range(1,101))
    shuffle(suitors)
    threshold = 37
    best_suitor = 1000
    for suitor in suitors[:threshold]:
        if suitor < best_suitor:
            best_suitor = suitor
    for suitor in suitors[threshold:]:
        if suitor < best_suitor:
            return suitor
    return suitors[-1]

NUM_SIMULATIONS = 100000

num_success = 0
for _ in range(NUM_SIMULATIONS):
    if marriage_problem() == 1:
        num_success += 1

print(num_success / NUM_SIMULATIONS)
