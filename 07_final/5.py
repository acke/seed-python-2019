from random import randint

def collided_ant_trial(n):
    collided = False
    init_direction = randint(0,1)
    for ant in range(n-1):
        direction = randint(0,1)
        if direction != init_direction:
            collided = True
            break
    return collided

n = 3
num_simulations = 100000
num_of_no_collided = 0
for i in range(num_simulations):
    if not collided_ant_trial(n):
        num_of_no_collided += 1
print("Percentage of collision:", num_of_no_collided/num_simulations)
