from random import randint

# Birthdays are from 1 to 365

def trial():
    birthdays = set()

    line_pos = 1
    while True:
        rand_birth = randint(1,365)
        if rand_birth in birthdays:
            return line_pos
        birthdays.add(rand_birth)
        line_pos += 1

NUM_SIM = 100000
freq = {}
for sim_num in range(NUM_SIM):
    if sim_num % 10000 == 0:
        print(sim_num)
    line_pos = trial()
    if line_pos in freq:
        freq[line_pos] += 1
    else:
        freq[line_pos] = 1

total = 0
for pos in freq:
    total += freq[pos] * pos

print(total / NUM_SIM)

