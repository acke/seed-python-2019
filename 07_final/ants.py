from random import randint

N = 3

def trial_coll():
    shape = [1] * N
    for i in range(N):
        choice = randint(0,1)
        shape[i] -= 1
        if choice == 0:
            shape[i-1] += 1
        else:
            shape[(i+1) % N] += 1

    for corner in shape:
        if corner > 1:
            return True
    return False

NUM_TRIALS = 1000000
num_coll = 0
for _ in range(NUM_TRIALS):
    if trial_coll():
        num_coll += 1

print(num_coll / NUM_TRIALS)

