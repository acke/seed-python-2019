from random import randint

num_boys = 0
num_girls = 0

initial_population = 100000
families_without_boys = initial_population

generation = 0
while families_without_boys > 0:
    for fam in range(families_without_boys):
        is_boy = randint(0,1)
        if is_boy:
            num_boys += 1
            families_without_boys -= 1
        else:
            num_girls += 1
    generation += 1
    print("Generation:", generation)
    print("Num boys:", num_boys)
    print("Num girls:", num_girls)
    print("Percentage boys:", num_boys / (num_boys + num_girls))
