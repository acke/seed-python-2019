from random import randint, shuffle

def run_trial(stay):
    """ Returns True if trial is success (car) else False """
    doors = ['G','G','C']
    shuffle(doors)
    door_idx = randint(0,2)
    my_door = doors[door_idx]
    del doors[door_idx]
    goat_idx_arr = []
    for index, door in enumerate(doors):
        if door == 'G':
            goat_idx_arr.append(index)
    shuffle(goat_idx_arr)
    del goat_idx_arr[0]
    other_door = doors[0]
    if stay:
        return my_door == 'C'
    else:
        return other_door == 'C'

num_simulations = 100000
num_stay_success = 0
for _ in range(num_simulations):
    if run_trial(stay=True):
        num_stay_success += 1
print(num_stay_success / num_simulations)
