# Homework Problems

# Start work on project

# Some more UVa
# Make sure all the previous ones are accepted!
# I included my solutions as reference in uva folder

# Hard Problems to try
# 11559 - Event Planning
# 10114 - Loansome Car Buyer
# 10141 - Request for Proposal
# 11507 - Bender B Rodriguez (use lots of if-else, simulation)

# (Easy-ish) Simulation Problems
# 00489 - Hangman Judge
# 10189 - Minesweeper
# 11459 - Snakes and Ladders

# Extra-Hard Simulation
# 00161 - Traffic Lights

