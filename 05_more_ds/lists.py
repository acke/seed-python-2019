# List Splicing

"""
Sum of two lists
a = [1,2,3]
b = [4,5,6]

print(a + b)
"""
a = [6,3,8,2,1,4]

# list of elements from pos 1 to 4 (exclusive)
print(a[1:4])

# list of elements from pos 1 to end
print(a[1:])

# list of elements from start to pos 4(exclusive)
print(a[:4])

# this is just the whole list
print(a[:3] + a[3:])

# this is sometimes seen
# why do this?
print(a[:])

a = [1,2,3]
b = a[:]
a[0] = 4
print(a)
print(b)

# reference.py

