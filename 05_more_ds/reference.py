# Consider this:
"""
a = 5
b = a
print(a)
print(b)

a = 9
print(a)
print(b)
"""

a = [1,2,3]
b = a
print(a)
print(b)

a[0] = 4
print(a)
print(b)

a = [4,5,6]
print(a)
print(b)

# imagine arr = a
def f(arr):
    arr[0] = 10

f(a)
print(a)


