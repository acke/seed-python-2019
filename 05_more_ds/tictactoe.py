def pretty_print(board):
    for i,row in enumerate(board):
        print("  ", end="")
        for j,elt in enumerate(row):
            print(elt,end="  ")
        print()

def is_valid_move(board, s):
    try:
        r, c = [int(i) for i in s.split()]
    except:
        return False
    if r < 0 or r > 2 or c < 0 or c > 2:
        return False
    if board[r][c] != '_':
        return False
    return True

def is_winning_board(board):
    # check rows
    for row in board:
        s = ''.join(row)
        if s == "OOO" or s == "XXX":
            return True

    # check cols
    for j in range(3):
        col = ""
        for i in range(3):
            col += board[i][j]
        if col == "OOO" or col == "XXX":
            return True

    # check diags
    diag1 = board[0][0] + board[1][1] + board[2][2]
    diag2 = board[0][2] + board[1][1] + board[2][0]
    if diag1 == "OOO" or diag1 == "XXX":
        return True
    if diag2 == "OOO" or diag2 == "XXX":
        return True

    return False

board = []
for _ in range(3):
    board.append(['_'] * 3)

turn = 0
while True:
    if turn == 9:
        print("DRAW!")
        break
    pretty_print(board)
    print("Enter move (row, col):")
    s = input()
    if is_valid_move(board, s):
        r, c = [int(i) for i in s.split()]
    else:
        continue
    if turn % 2 == 0:
        board[r][c] = 'X'
    else:
        board[r][c] = 'O'
    if is_winning_board(board):
        pretty_print(board)
        if turn % 2 == 0:
            print("PLAYER 1 (X) WINS!")
        else:
            print("PLAYER 2 (O) WINS!")
        break
    turn += 1

