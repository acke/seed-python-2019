# Basic Data Types
# string (str): letters, characters
# numbers (int or float)
# boolean (bool): True or False

# Conditionals
# if 
if 5 < 10:
    print("Hi")

# Loops
# while
i = 1
while i < 10:
    print(i)
    i += 1

# for
for i in range(10):
    print(i)

