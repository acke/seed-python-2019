# Homework due in 2 weeks
# Read the Git Pro book chapter 1 and 2
# Practice the git commands along with terminal commands

# Programming Challenges
# Upload to your own repos and I can check them remotely

# 1. Write a program to compute the sum of the first n numbers
# We did this in class already, but try to write it from scratch
# WITHOUT referring to code
# n = 3: 1 + 2 + 3 = 6

total = 0
n = 100
for i in range(1,n+1):
    total += i
print(total)
print(n*(n+1)//2)

# 2. Write a program to compute the sum of the first n EVEN numbers.
# n = 3: 2 + 4 + 6 = 12
total = 0
n = 100
for i in range(n+1):
    total += 2*i
print(total)
print(n*(n+1))

# 3. Write a program to compute the sum of the first n ODD numbers.
# n = 3: 1 + 3 + 5 = 9

total = 0
n = 100
for i in range(n):
    total += 2*i+1
print(total)
print(n*n)

# 4. Write a program to compute the following, given n:
# n = 3: 3^30 + 3^29 + 3^28 + ... + 3^2 + 3^1 + 3^0
# do this with a loop (not hardcoded)

total = 0
n = 3
for i in range(31):
    total += n**i
print(total)

# 5. Try to work on the prime number program more.
# Given n, determine if n is prime.

# Extra Credit: for the problems above, see if you 
# can solve them faster than using a loop (closed form formula)

