# Terminal Commands

- pwd: paste working directory
- ls: list contents
- cd: change directory
	- to go back up a level, "cd .."
	- .. represents parent
	- . represents current
	- ~ represents home
- mkdir: make directory "mkdir foldername"
- rmdir: remove directory "rmdir foldername"
	- this only works for empty folder
- touch: create empty file "touch filename"
- rm: remove "rm filename"
- rm -r : "rm -r foldername"
	- this works for folders with stuff in it
- mv: "mv file1 file2" renames file1 to file2
	- can also move files from one location to another

% Practice: create a folder called "test"
% Create a file called "hello.txt" in "test"
% Create another called "hello2.txt" in "test"
% Delete "hello.txt"
% Rename "hello2.txt" to "hello1.txt"
% Delete whole test folder

