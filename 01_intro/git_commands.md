# Git commands
- git clone url
	- this gets the repo from the server
- git status
	- can show you the status of git
- git add .
	- you need to specify what you are adding
	- recall . means current directory as an example
- git commit -m "Message here describing changes"
- git push
	- pushes change to server
- git pull
	- if changes were made to server, retrieve them

# The Flow
1. Write your code and save to your repo
2. Run `git add` on the files you want to add
3. Run `git commit -m "message"` to commit
4. Run `git push` to push to your remote repository
