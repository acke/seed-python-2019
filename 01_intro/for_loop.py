# Warmup Exercise: Write a program, given a number,
# print out all multiples of 2 and 3 less than that 
# number in increasing order (starting from 0)

n = int(input())
for i in range(n):
    if i % 2 == 0 or i % 3 == 0:
        print(i)

# Exercise: Given an input integer n,
# output n^3 + n^2 + n + 1
# output n^7 + n^6 + n^4 + n^3 + n^2 + n + 1
# output n^21 + n^20 + .. + n^2 + n + 1
# output n^30 + n^29 + .. + n^2 + n + 1
# n^e + n^{e-1} + ... + n^2 + n + 1

# Given n, print out the sum of the first n numbers
# n = 5: 15
# n*(n+1) // 2

n = int(input())
total = 0
for i in range(1,n+1):
    total += i**2
print(total)

# Given n, print out the sum of the first n numbers squared
# n = 5: 1 + 4 + 9 + 16 + 25 = 55

# Given n, print out the sum of the first n numbers cubed

# Given n, print out the sum of the first n numbers to some
# power e

