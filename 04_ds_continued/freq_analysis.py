# Frequency table on python_para letters
# Determine the order of most frequent to least frequent letters. 
# Output the frequencies as a percentage out of 100.

from string import ascii_lowercase

def freq_analysis(filename):
    freq = {}

    for letter in ascii_lowercase:
        freq[letter] = 0

    book = open(filename,encoding="utf-8")
    python_para = book.read()

    for c in python_para:
        if not c.isalpha():
            continue
        c = c.lower()
        if c not in freq:
            continue
        freq[c] += 1

    for c in freq:
        freq[c] /= len(python_para)
        freq[c] *= 100

    sorted_freq = []
    for c in freq:
        sorted_freq.append((freq[c], c))
    sorted_freq.sort(reverse=True)

    for freq, c in sorted_freq:
        print(" ", c, freq)
    book.close()
    return sorted_freq

freq_analysis("cipher.txt")
