# Work on some UVa problems
# uva.onlinejudge.org

# 11792 - Relational Operators
# 11498 - Division of Nlogonia
# 11727 - Cost Cutting
# 11764 - Jumping Mario
# 11799 - Horror Dash
# 573 - The Snail

# Shortcuts

# List comprehension trick, will discuss further in future
# if reading input like so: 123 456
# a, b = [int(i) for i in input().split()]

