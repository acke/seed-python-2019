# Frequency table on python_para letters
# Determine the order of most frequent to least frequent letters. 
# Output the frequencies as a percentage out of 100.

from string import ascii_uppercase

def freq_analysis(filename):
    freq = {}

    for letter in ascii_uppercase:
        freq[letter] = 0

    book = open(filename,encoding="utf-8")
    python_para = book.read()

    for c in python_para:
        if not c.isalpha():
            continue
        c = c.upper()
        if c not in freq:
            continue
        freq[c] += 1

    for c in freq:
        freq[c] /= len(python_para)
        freq[c] *= 100

    sorted_freq = []
    for c in freq:
        sorted_freq.append((freq[c], c))
    sorted_freq.sort(reverse=True)

    #for freq, c in sorted_freq:
    #    print(" ", c, freq)
    book.close()
    return sorted_freq


sorted_freq = freq_analysis("myvillage.txt")
freq_letters = []
for freq, c in sorted_freq:
    freq_letters.append(c)

sorted_freq_cipher = freq_analysis("cipher.txt")
cipher_freq_letters = []
for freq, c in sorted_freq_cipher:
    cipher_freq_letters.append(c)

print(freq_letters)
print(cipher_freq_letters)
conv = {}
for i in range(len(freq_letters)):
    conv[cipher_freq_letters[i]] = freq_letters[i]

conv['T'] = 'N'
conv['R'] = 'S'
conv['P'] = 'H'
conv['B'] = 'I'
conv['C'] = 'O'
conv['H'] = 'K'
conv['I'] = 'F'
conv['U'] = 'U'
conv['W'] = 'G'
conv['L'] = 'M'
conv['S'] = 'J'
conv['E'] = 'Y'

with open("cipher.txt",encoding="utf-8") as cipher:
    orig = cipher.read()
    new = ""
    for c in orig:
        if c in conv:
            new += conv[c]
        else:
            new += c
    print(orig)
    print()
    print(new)

